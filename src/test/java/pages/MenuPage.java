package pages;

import org.openqa.selenium.WebDriver;

public class MenuPage extends GenericPage {
    public MenuPage(WebDriver driver) {
        super(driver);
    }

    public TransfersPage openTransfersPage() {
        findElementByLinkText("TRANSFER").click();
        return new TransfersPage(driver);
    }

    public ProfilePage openProfilePage() {
        findElementByLinkText("PROFILE").click();
        return new ProfilePage(driver);
    }
}
