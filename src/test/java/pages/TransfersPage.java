package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TransfersPage extends GenericPage {
    public static final String TRANSFER_ACCOUNT = "transfer-account";
    public static final String TRANSFER_TITLE_ID = "transfer-title";
    public static final String TRANSFER_RECIPENT_ID = "transfer-recipent";
    public static final String TRANSFER_AMOUNT_ID = "transfer-amount";
    public static final String SEND_TRANSFER_BUTTON_CSS = "input[value='Send']";
    public static final String TRANSFER_TYPE_STANDARD_CSS = "label[for='transfer-type-standard']";
    public static final String TRANSFER_TYPE_FAST_CSS = "label[for='transfer-type-fast']";
    public static final String TRANSFER_TYPE_INSTANT_CSS = "label[for='transfer-type-instant']";
    public static final String PENDING_TRANSFER_AMOUNT = "//*[@id=\"transfer-pending-table\"]/tbody/tr[1]/td[3] ";
    public static final int LAST_TRANSFER_AMOUNT = 2;
    public static final int PENDING_TRANSFER_TABLE_SIZE = 3;

    public enum TRANSFER_TYPE {
        STANDARD, FAST, INSTANT
    }

    public enum PENDING_TRANSFER_HEADING {
        DATE, TITLE, AMOUNT
    }

    public TransfersPage(WebDriver driver) {
        super(driver);
    }

    public TransfersPage fillAccountNumber(String accountNumber) {
        findElementByID(TRANSFER_ACCOUNT).sendKeys(accountNumber);
        return this;
    }

    public TransfersPage fillTransferTitle(String transferTitle) {
        findElementByID(TRANSFER_TITLE_ID).sendKeys(transferTitle);
        return this;
    }

    public TransfersPage fillTransferRecipent(String transferRecipent) {
        findElementByID(TRANSFER_RECIPENT_ID).sendKeys(transferRecipent);
        return this;
    }

    public TransfersPage fillTransferAmount(String transferAmount) {
        findElementByID(TRANSFER_AMOUNT_ID).sendKeys(transferAmount);
        return this;
    }

    public String getAccountNumberValue() {
        return findElementByID(TRANSFER_ACCOUNT).getAttribute("value");
    }

    public String getPendingTransferAmount() {
        return findElementByXpath(PENDING_TRANSFER_AMOUNT).getText();
    }

    public TransfersPage sendTransfer() {
        waitUntilElementVisibleByCSS(SEND_TRANSFER_BUTTON_CSS);
        findElementByCSS(SEND_TRANSFER_BUTTON_CSS).click();
        return this;
    }

    public TransfersPage selectTransferType(TRANSFER_TYPE transfer_type) {
        switch (transfer_type) {
            case STANDARD:
                findElementByCSS(TRANSFER_TYPE_STANDARD_CSS).click();
                return this;
            case FAST:
                findElementByCSS(TRANSFER_TYPE_FAST_CSS).click();
                return this;
            case INSTANT:
                findElementByCSS(TRANSFER_TYPE_INSTANT_CSS).click();
                return this;
        }
        return this;
    }

    public List<String> getAllTransfers() {
        List<String> transfersStringList = new ArrayList<String>();
        List<WebElement> transferPendingWebElementList = new ArrayList<WebElement>();
        transferPendingWebElementList.
                addAll(driver.findElement(By.id("transfer-pending-table")).findElements(By.tagName("td")));
        for (WebElement element : transferPendingWebElementList) {
            transfersStringList.add(element.getText());
        }
        return transfersStringList;
    }

    public String getLastTransferAmount() {
        return getAllTransfers().get(LAST_TRANSFER_AMOUNT);
    }

    public Map<PENDING_TRANSFER_HEADING, String> getLastTransferMap() {
             Map<PENDING_TRANSFER_HEADING, String> lastPendingTransfer = new HashMap<>();
             lastPendingTransfer.put(PENDING_TRANSFER_HEADING.DATE,getAllTransfers().get(0));
             lastPendingTransfer.put(PENDING_TRANSFER_HEADING.TITLE,getAllTransfers().get(1));
             lastPendingTransfer.put(PENDING_TRANSFER_HEADING.AMOUNT,getAllTransfers().get(2));
             return lastPendingTransfer;
    }

    public Map<Integer, Map<PENDING_TRANSFER_HEADING, String>> getAllTransfersMap() {
        Map<Integer, Map<PENDING_TRANSFER_HEADING, String>> allTransfersMap = new HashMap<>();
        List<String> allPendingTransfers = getAllTransfers();
        if(allPendingTransfers.size() > 0) {
            for (int i = 0; i < allPendingTransfers.size(); i += PENDING_TRANSFER_TABLE_SIZE) {
                Map<PENDING_TRANSFER_HEADING, String> rowTransferMap = new HashMap<>();
                rowTransferMap.put(PENDING_TRANSFER_HEADING.DATE, allPendingTransfers.get(i));
                rowTransferMap.put(PENDING_TRANSFER_HEADING.TITLE, allPendingTransfers.get(i + 1));
                rowTransferMap.put(PENDING_TRANSFER_HEADING.AMOUNT, allPendingTransfers.get(i + 2));
                allTransfersMap.put(i/PENDING_TRANSFER_TABLE_SIZE, rowTransferMap);
            }
        }
        return allTransfersMap;
    }

    public String getPendingTransferData(Integer pendingTransferRow, PENDING_TRANSFER_HEADING pendingTransferColumn) {
        return getAllTransfersMap().get(pendingTransferRow).get(pendingTransferColumn);
    }
}