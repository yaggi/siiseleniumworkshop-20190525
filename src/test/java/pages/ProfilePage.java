package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfilePage extends GenericPage {
    public ProfilePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "update-name")
    WebElement profileName;

    @FindBy(id = "update-street")
    WebElement updateStreet;

    @FindBy(id = "update-city")
    WebElement updateCity;

    @FindBy(id = "update-postalcode")
    WebElement updatePostalCode;

    @FindBy(id = "update-email")
    WebElement updateEmail;

    @FindBy(id = "update-country")
    WebElement updateCountry;

    @FindBy(css = "label[for='update-gender-male']")
    WebElement updateGenderMale;

    @FindBy(css = "label[for='update-gender-female']")
    WebElement updateGenderFemale;

    @FindBy(css = "label[for='update-gender-dont']")
    WebElement updateGenderDont;

    @FindBy(css = "label[for='update-newsletter']")
    WebElement updateNewsletter;

    @FindBy(id = "update-newsletter")
    WebElement updateNewsletterState;

    @FindBy(id = "update-additional")
    WebElement updateAdditionalMessage;

    @FindBy(css = "input[value='Saved']")
    WebElement sendButton;

    public enum GENDER_TYPE {
        MALE, FEMALE, DONTASK
    }

    public ProfilePage updateName(String newProfileName) {
        profileName.sendKeys(newProfileName);
        return this;
    }

    public ProfilePage updateCity(String newCity) {
        updateCity.sendKeys(newCity);
        return this;
    }

    public ProfilePage updateStreet(String newStreet) {
        updateStreet.sendKeys(newStreet);
        return this;
    }

    public ProfilePage updatePostalcode(String newPostalcode) {
        updatePostalCode.sendKeys(newPostalcode);
        return this;
    }

    public ProfilePage updateEmail(String newEmail) {
        updateEmail.sendKeys(newEmail);
        return this;
    }

    public ProfilePage updateCountry(String dropdownCountry) {
        Select countryDropdown = new Select(updateCountry);
        countryDropdown.selectByVisibleText(dropdownCountry);
        return this;
    }

    public ProfilePage selectGender(GENDER_TYPE gender_type) {
        switch (gender_type) {
            case MALE:
                updateGenderMale.click();
                return this;
            case FEMALE:
                updateGenderFemale.click();
                return this;
            case DONTASK:
                updateGenderDont.click();
                return this;
        }
        return this;
    }

    public ProfilePage deselectNewsletter() {
        if(updateNewsletterState.isSelected()) {
            updateNewsletter.click();
        }
        return this;
    }

    public ProfilePage selectNewsletter() {
        if(!updateNewsletterState.isSelected()) {
            updateNewsletter.click();
        }
        return this;
    }

    public ProfilePage addAdditionalMessage(String newAdditionalMessage) {
        updateAdditionalMessage.sendKeys(newAdditionalMessage);
        return this;
    }

    public ProfilePage sendUpdatedProfile() {
        fluentWaitForElementIsDisplayed(sendButton);
        sendButton.click();
        return this;
    }

    public Map<String, String> getPersonalDataMap() {
        Map<String, String> personalDataMap = new HashMap<>();
        List<WebElement> columnHeading = new ArrayList<>();
        List<WebElement> profileValues = new ArrayList<>();
        profileValues.addAll(findElementByID("personal-data-table").findElements(By.tagName("td")));
        columnHeading.addAll(findElementByID("personal-data-table").findElements(By.tagName("th")));
        for(int i = 0; i < columnHeading.size(); i++) {
            personalDataMap.put(columnHeading.get(i).getText(), profileValues.get(i).getText());
        }
        return  personalDataMap;
    }

    public String getUpdatedPersonalData(String personalData){
        return getPersonalDataMap().get(personalData);
    }

}
