package tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import pages.MenuPage;
import pages.TransfersPage;

import java.util.concurrent.TimeUnit;

import static helpers.Configuration.getConfiguration;
import static helpers.Driver.initializeWebDriver;
import static org.junit.Assert.assertEquals;
import static tests.TransferTestData.*;

public class TransferTest {
    private WebDriver driver;

    @Before
    public void setUp() {
        driver = initializeWebDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(getConfiguration().getSiteURL());
    }

    @Test
    public void sendTransferTest() {
        MenuPage menuPage = new MenuPage(driver);
        TransfersPage transfersPage = menuPage.openTransfersPage()
                .fillAccountNumber(TRANSFER_ACCOUNT_NUMBER)
                .fillTransferTitle(TRANSFER_TITLE)
                .fillTransferRecipent(TRANSFER_RECIPENT)
                .fillTransferAmount(TRANSFER_AMOUNT)
                .selectTransferType(TransfersPage.TRANSFER_TYPE.INSTANT)
                .sendTransfer();
        assertEquals("Amount not correct", TRANSFER_AMOUNT,
                transfersPage.getPendingTransferData(TRANSFER_TEST_PENDING_TRANSFER_ROW,
                        TransfersPage.PENDING_TRANSFER_HEADING.AMOUNT));
        assertEquals("Title not correct", TRANSFER_TITLE,
                transfersPage.getPendingTransferData(TRANSFER_TEST_PENDING_TRANSFER_ROW,
                        TransfersPage.PENDING_TRANSFER_HEADING.TITLE));
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
