package tests;

public interface TransferTestData {
    String MAIN_PAGE_TITLE = "Selenium Workshop - main";
    String TRANSFERS_PAGE_TITLE = "Selenium Workshop - Transfer";
    String TRANSFER_ACCOUNT_NUMBER = "242342342";
    String TRANSFER_TITLE = "Szkolenie Selenium";
    String TRANSFER_RECIPENT = "Jan Kowalski";
    String TRANSFER_AMOUNT = "100";

    int TRANSFER_TEST_PENDING_TRANSFER_ROW = 0;
}
