package tests;

import pages.ProfilePage;

public interface ProfileTestData {
    String NEW_PROFILE_NAME = "Zdzislaw";
    String NEW_STREET = "Wielicka";
    String NEW_CITY = "Kraków";
    String NEW_POSTALCODE = "32-080";
    String NEW_EMAIL = "mjagoda@sii.pl";
    String DROPDOWN_COUNTRY = "Sweden";
    ProfilePage.GENDER_TYPE GENDER_TYPE = ProfilePage.GENDER_TYPE.FEMALE;
    String NEW_ADDITIONAL_MESSAGE = "This is additional message";
}
