package tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import pages.MenuPage;
import pages.ProfilePage;

import static helpers.Configuration.getConfiguration;
import static helpers.Driver.initializeWebDriver;
import static org.junit.Assert.assertEquals;
import static tests.ProfileTestData.*;

public class ProfileTest {
    private WebDriver driver;

    @Before
    public void setUp() {
        driver = initializeWebDriver();
        driver.get(getConfiguration().getSiteURL());
    }

    @Test
    public void updateProfileTest() {
        MenuPage menuPage = new MenuPage(driver);
        ProfilePage profilePage = menuPage.openProfilePage()
                .updateName(NEW_PROFILE_NAME)
                .updateStreet(NEW_STREET)
                .updateCity(NEW_CITY)
                .updatePostalcode(NEW_POSTALCODE)
                .updateEmail(NEW_EMAIL)
                .updateCountry(DROPDOWN_COUNTRY)
                .selectGender(GENDER_TYPE)
                .deselectNewsletter()
                .addAdditionalMessage(NEW_ADDITIONAL_MESSAGE)
                .sendUpdatedProfile();
        assertEquals("Name doesn't match", NEW_PROFILE_NAME,
                profilePage.getUpdatedPersonalData("Name"));
        assertEquals("Street doesn't match", NEW_STREET,
                profilePage.getUpdatedPersonalData("Street"));
        assertEquals("City doesn't match", NEW_STREET,
                profilePage.getUpdatedPersonalData("Street"));
        assertEquals("Postal code doesn't match", NEW_POSTALCODE,
                profilePage.getUpdatedPersonalData("Postal code"));
        assertEquals("Email doesn't match", NEW_EMAIL,
                profilePage.getUpdatedPersonalData("Email"));
        assertEquals("Country doesn't match", DROPDOWN_COUNTRY,
                profilePage.getUpdatedPersonalData("Country"));
        assertEquals("Gender doesn't match", GENDER_TYPE.toString().toLowerCase(),
                profilePage.getUpdatedPersonalData("Gender").toLowerCase());
        assertEquals("Note doesn't match", NEW_ADDITIONAL_MESSAGE,
                profilePage.getUpdatedPersonalData("Note"));
    }

    @After
    public void tearDown() {
        driver.quit();
    }

}
