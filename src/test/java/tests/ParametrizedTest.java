package tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.MenuPage;
import pages.TransfersPage;

import java.util.concurrent.TimeUnit;

import static helpers.Configuration.getConfiguration;
import static helpers.Driver.initializeWebDriver;
import static org.junit.Assert.assertEquals;
import static tests.TransferTestData.*;

public class ParametrizedTest {
    private WebDriver driver;

    @BeforeEach
    public void setUp() {
        driver = initializeWebDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(getConfiguration().getSiteURL());
    }

    @ParameterizedTest
    @ValueSource(strings = {"100000", "999999", "!@@#$$%%^&*(", "alert(blablabla)"})
    public void sendTransferTest(String parameter) {
        MenuPage menuPage = new MenuPage(driver);
        menuPage.openTransfersPage()
                .fillAccountNumber(parameter);
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }

}
